const express = require('express');
const axios = require('axios').default;
const cron = require("node-cron");
const { compose, not, isEmpty, find, path, prop, head } = require('ramda');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const cors = require('cors');
const { format } = require('date-fns');
const {PythonShell} = require('python-shell');
const bodyParser = require('body-parser');

const PORT = 2000;
const ROUTER_URL = 'http://192.168.1.1/cgi-bin-igd/netcore_get.cgi';
const WLAN5G_NETWORK_TYPE = 'WLAN5G';
const DB_URL = 'mongodb://localhost:27017';
const DB_NAME = 'network_measure';
const dbClientOptions = { useUnifiedTopology: true };
const DATE_FORMAT = 'MM-dd-yyyy';
const HOUR_FORMAT = 'HH:00';

const app = express();
const dbClient = new MongoClient(DB_URL, dbClientOptions);

const corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions))
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

const getResponseStatisticsData = (response) => response.status === 200 
  ? path(['data', 'statistics_list'], response) 
  : undefined;

const getNetworkStats = async () => {
  try {
    const stats = await axios.get(ROUTER_URL).then(getResponseStatisticsData);
    console.info('Successfully stats request');
    const trafficData = await getWlan5GTrafficData(stats);
    return trafficData;
  } catch (error) {
    console.error('Error while receiving network stats', error);
    return undefined;
  }
};

const notEmpty = compose(not, isEmpty);
const isValidArray = array => Array.isArray(array) && notEmpty(array);
const byWLAN5GType = ({type}) => type === WLAN5G_NETWORK_TYPE;

const getRxBytes = compose(
  Number,
  prop('rx_bytes')
);

const getFormattedDate = (date, dateFormat) => format(
  date,
  dateFormat
);

const getWlan5GTrafficData = async (stats) => {
  if (isValidArray(stats)) {
    const wlan5gStats = find(byWLAN5GType, stats);
    const rx_bytes = getRxBytes(wlan5gStats);
    if (rx_bytes) {
      const currentDate = new Date();
      const date = getFormattedDate(currentDate, DATE_FORMAT);
      const hour = getFormattedDate(currentDate, HOUR_FORMAT);
      return {
        rx_bytes,
        date,
        hour
      };
    }
  };
  return undefined;
};

const onDbConnect = (err) => {
  assert.equal(null, err);
  console.info("Connected successfully to server");
};

dbClient.connect(onDbConnect);

const db = dbClient.db(DB_NAME);
const collection = db.collection('daily_measure');

const saveStats = (networkStats) => {
  if (networkStats) {
    try {
      collection.insertOne(networkStats);
    } catch (error) {
      console.error('Error while inserting measures to DB', error);
    }
  }
};

cron.schedule("0 * * * *", async () => {
    const networkStats = await getNetworkStats();  
    saveStats(networkStats);
});

app.get('/', async (req, res) => {
  collection.find({}).toArray((e, statistics) => {
    if (e) throw e;
    res.json(statistics)
  }); 
});



app.post('/hurst', (req, res) => {
  const hurstOptions = {
    args: req.body.measures
  }
  PythonShell.run('./lib/get_hurst.py', hurstOptions, function (err, results) {
    if (err) {
      res.send({
        hurst: 0
      });
    };
    res.send({
      hurst: results[0]
    });
  });

});

app.listen(PORT, () => console.info(`App listening at http://localhost:${PORT}`));