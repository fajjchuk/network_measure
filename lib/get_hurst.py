import sys
import numpy as np
import matplotlib.pyplot as plt
from hurst import compute_Hc

del sys.argv[0]

def convert_array(list):
  array_values = []

  for i in list: 
    array_values.append(int(i))

  return array_values

series = convert_array(sys.argv)

H, c, data = compute_Hc(series, kind='change', simplified=True)

print(H)